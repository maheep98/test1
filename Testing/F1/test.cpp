#include <gtest/gtest.h>
#include "opposite_number.cpp"

TEST(NumTest, PosNos) {
    ASSERT_EQ(-1, opposite(1));
    ASSERT_EQ(-3, opposite(3));
    ASSERT_EQ(-5, opposite(5));
    
}

TEST(NumTest, NegNos) {
    ASSERT_EQ(1, opposite(-1));
    ASSERT_EQ(3, opposite(-3));
    ASSERT_EQ(5, opposite(-5));
    
}



int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
