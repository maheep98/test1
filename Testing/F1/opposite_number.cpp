/*
Very simple, given a number, find its opposite.

Examples:

1: -1
14: -14
-34: 34
*/
#include"opposite_number.hpp"
int opposite(int number) 
{
  int ans;
  ans=number*-1;
  return ans;
}
